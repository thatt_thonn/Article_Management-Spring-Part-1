
create table tbl_category
(
	id int primary key auto_increment,
	name varchar(50)
);


create table tbl_article
(
	id int primary key auto_increment,
	title varchar(50),
	author varchar(15),
	des text,
	create_date date,
	image text,
	category_id int references tbl_category(id)
);

insert into tbl_category(name) values('Love'),('Romantic'),('sadness'),('Poem'),('History');

insert into tbl_article(title,author, des, create_date,image, category_id)
values('Five Step Deciding Your Mind','Nana Ly','General Decission',NOW(),'/image/1c92526c-854c-4357-8e6d-2b670ed31ee9..PNG',1),
('Five Step Deciding Your Mind','Nana Ly','General Decission',NOW(),'/image/5b31f538-cd7b-42a8-9f52-95640890162f..PNG',1),
('Belove everyone','Koko','General Decission',NOW(),'/image/bfb4b333-60a6-4b24-a0e4-29114e231334..png',2),
('Five Step Deciding Your Mind','Rosa','General Decission',NOW(),'/image/d2bf5974-36f5-4414-b089-7531bf6fe338..png',2);