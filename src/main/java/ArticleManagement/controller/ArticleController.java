package ArticleManagement.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

import javax.swing.JOptionPane;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import ArticleManagement.model.Article;
import ArticleManagement.model.Category;
import ArticleManagement.service.article.ArticleService;
import ArticleManagement.service.category.CategoryServices;

@Controller
@PropertySource("classpath:location.properties")
public class ArticleController {

	private ArticleService articleService;
	private int id = 1;
	private CategoryServices categoryServices;

	@Value("${server.img.path.folder}")
	private String serverPath;

	@Autowired
	public void setArticleService(ArticleService articleService) {
		this.articleService = articleService;
	}
	
	@Autowired
	public void setCategoryServices(CategoryServices categoryServices) {
		this.categoryServices = categoryServices;
	}

	@GetMapping({ "/findAll", "" })
	public String findAllArticle(ModelMap model) {
		model.addAttribute("article", articleService.findAllArticles());
		return "articles/index";
	}

	@GetMapping("/add")
	public String add(@Valid ModelMap model) {
		model.addAttribute("article", new Article());
		model.addAttribute("allCategory", categoryServices.findAll());
		model.addAttribute("condition", true);
		return "articles/add";
	}

	@PostMapping("/add/submit")
	public String saveAddArticle(ModelMap model, @Valid @ModelAttribute Article article, BindingResult result,
			@RequestParam("img") List<MultipartFile> multipartFile) {
		System.out.println("Error: " + multipartFile.isEmpty());
		if (result.hasErrors()) {
			model.addAttribute("article", article);
			model.addAttribute("allCategory", categoryServices.findAll());
			return "articles/add";
		}
		if (!multipartFile.isEmpty()) {
			multipartFile.forEach(file -> {
				try {
					String generatePath = UUID.randomUUID().toString() + "."
							+ file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
					Files.copy(file.getInputStream(), Paths.get(serverPath, generatePath));
					article.setImage("/image/" + generatePath);
				} catch (IOException e) {
					e.printStackTrace();
				}
			});
		}
		
		article.setId(id);
		article.setCreateDate(LocalDate.now().toString());
		article.setCategory(categoryServices.findOneCategory(article.getCategory().getId()));
		articleService.addArticle(article);
		System.out.println(article.getCategory().getId()+" : "+article.getCategory().getName());
		id++;
		return "redirect:/findAll";
	}

	@GetMapping("/update/{id}")
	public String updateOneArticle(ModelMap model, @PathVariable Integer id) {
		Article article = articleService.findArticleOne(id);
		model.addAttribute("article", article);
		model.addAttribute("allCategory", categoryServices.findAll());
		return "articles/update";
	}

	@PostMapping("/update/submit")
	public String saveUpdateArticle(ModelMap model, @Valid @ModelAttribute Article article, BindingResult result,
			@RequestParam("img") MultipartFile multipartFile) {
		System.out.println(article.getCategory().getId());
		if (result.hasErrors()) {		
//			model.addAttribute("article", article);
			model.addAttribute("allCategory", categoryServices.findAll());
			model.addAttribute("article", article);
			return "articles/update";
		}
		else {
			Category category = categoryServices.findOneCategory(article.getCategory().getId());
			article.setCategory(category);
		}
		if (!multipartFile.isEmpty()) {
			String generatePath = UUID.randomUUID().toString() + "." + multipartFile.getOriginalFilename()
					.substring(multipartFile.getOriginalFilename().lastIndexOf("."));
			try {
				Files.copy(multipartFile.getInputStream(), Paths.get(serverPath, generatePath));
			} catch (IOException e) {
				e.printStackTrace();
			}
			article.setImage("/image/" + generatePath);
			
		}
		if (articleService.updateArticle(article)) {
			System.out.println("Error Article: "+article.getCategory().getId());
			System.out.println("Data was update...");
		}
		return "redirect:/findAll";
	}

	@GetMapping("/delete/{id}")
	public String deleteOneArticle(@PathVariable Integer id) {
		if (articleService.deleteOneArticle(id)) {
			System.out.println("Data was delete..");
		}
		return "redirect:/findAll";
	}

	@GetMapping("/view/{id}")
	public String viewOneArticle(@PathVariable int id, ModelMap model) {
		Article article = articleService.findArticleOne(id);
		model.addAttribute("article", article);
		return "articles/view";
	}
	
}
