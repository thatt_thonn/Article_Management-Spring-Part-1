package ArticleManagement.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import ArticleManagement.model.Category;
import ArticleManagement.repository.NewRepository;
import ArticleManagement.service.category.CategoryServices;

@Controller
public class CategoryController {
	
	private CategoryServices categoryServices;
	private Integer id=1;
	
	@Autowired
	public void setCategoryServices(CategoryServices categoryServices) {
		this.categoryServices = categoryServices;
	}
	
	
	@GetMapping("/category")
	public String addCategory(ModelMap model) {
		model.addAttribute("cat", new Category());
		return "category/add";
	}
	
	@PostMapping("/add/submit/category")
	public String SaveAddCategory(ModelMap model, @Valid @ModelAttribute Category category, BindingResult result) {
		if(result.hasErrors()) {
			model.addAttribute("cat", category);	
			model.addAttribute("error", result.hasErrors());
			return "category/add";
		}
		category.setId(id);
		
		categoryServices.addCategory(category);
		id++;
		System.out.println(category.getName());
		return "redirect:/allcategory";
	}
	
	@GetMapping({"/allcategory",""})
	public String listAllCategory(ModelMap model) {
		model.addAttribute("cat", categoryServices.findAll());
		return "category/index";
	}
	
	@GetMapping("/update/category/{id}")
	public String updateCategory(ModelMap model, @PathVariable int id) {
		model.addAttribute("cat", categoryServices.findOneCategory(id));
		return "category/update";
	}
		
	@PostMapping("/submit/update/category")
	public String submitUpdateCategory(ModelMap model,@ModelAttribute Category category, BindingResult result) {
		System.out.println(category.getId()+category.getName());
		if(result.hasErrors()) {
			model.addAttribute("cat", category);
			model.addAttribute("error", result.hasErrors());
			return "category/add";
		}
		if(categoryServices.updateCategory(category)) {
			System.out.println("Data Was update...");
		}
		return "redirect:/allcategory";
	}

	@GetMapping("/delete/category/{id}")
	public String deleteCategory(@PathVariable int id) {
		categoryServices.deleteCategory(id);
		return "redirect:/allcategory";
	}
	
	@Autowired
	private NewRepository repo;
	@GetMapping("/show")
	public String showCategory() {
		repo.show();
		return null;
	}
}
