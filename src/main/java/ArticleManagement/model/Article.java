package ArticleManagement.model;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

public class Article {

	private int id;
	@NotBlank
	private String title;
	@NotBlank
	private String author;
	@NotBlank
	private String des;
	private Category category;
	private String createDate;
	
	
	private String image;
	
	public Article() {
		
	}

	public Article(int id, String title, String author, String des, Category category, String createDate, String image) {
		super();
		this.id = id;
		this.title = title;
		this.author = author;
		this.des = des;
		this.category = category;
		this.createDate = createDate;
		this.image=image;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getDes() {
		return des;
	}

	public void setDes(String des) {
		this.des = des;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	@Override
	public String toString() {
		return "Article [id=" + id + ", title=" + title + ", author=" + author + ", des=" + des + ", category="
				+ category + ", createDate=" + createDate + ", image=" + image + "]";
	}
	
	
	
	
	
}
