package ArticleManagement.repository.articles;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import ArticleManagement.model.Article;

//@Repository
public class ArticleRepositoryImp implements ArticleRepository{

	List<Article> listArticle=new ArrayList<Article>();
	
	@Override
	public void addArticle(Article article) {
		listArticle.add(article);		
	}

	@Override
	public List<Article> findAllArticles() {
		return listArticle;
	}

	@Override
	public boolean updateArticle(Article article) {
		for(int i=0;i<listArticle.size();i++) {
			if(listArticle.get(i).getId()==article.getId()) {
				listArticle.set(i, article);
				return true;
			}
		}
		return false;
	}

	@Override
	public Article findArticleOne(int id) {
		for(int i=0;i<listArticle.size();i++) {
			if(listArticle.get(i).getId()==id) {
				return listArticle.get(i);
			}
		}
		return null;
	}

	@Override
	public boolean deleteOneArticle(int id) {
		for(int i=0;i<listArticle.size();i++) {
			if(listArticle.get(i).getId()==id) {
				listArticle.remove(i);
				return true;
			}
		}
		return false;
	}





	
}
