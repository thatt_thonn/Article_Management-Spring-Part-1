package ArticleManagement.repository.articles;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import ArticleManagement.model.Article;


@Repository
public interface ArticleRepository {
	
	@Insert("insert into tbl_article(title,author, des, create_date,image, category_id)values(#{title},#{author},#{des},NOW(),#{image},#{category.id})")
	public void addArticle(Article article);
	
	@Select("select a.id, a.title, a.author, a.des, a.create_date, a.image, a.category_id, c.name from tbl_article a inner join tbl_category c on a.category_id=c.id")
	@Results({
		@Result(property="id",column="id"),
		@Result(property="title",column="title"),
		@Result(property="author",column="author"),
		@Result(property="des",column="des"),
		@Result(property="createDate",column="create_date"),
		@Result(property="image",column="image"),
		@Result(property="category.id",column="category_id"),
		@Result(property="category.name",column="name")
	})
	public List<Article> findAllArticles();
	
	@Select("select a.id, a.title, a.author, a.des, a.create_date, a.image, a.category_id, c.name from tbl_article a inner join tbl_category c on a.category_id=c.id where a.id=#{id}")
	@Results({
		@Result(property="id",column="id"),
		@Result(property="title",column="title"),
		@Result(property="author",column="author"),
		@Result(property="des",column="des"),
		@Result(property="createDate",column="create_date"),
		@Result(property="image",column="image"),
		@Result(property="category.name",column="name")
	})
	public Article findArticleOne(int id);
	
	@Update("update tbl_article set title=#{title}, author=#{author},des=#{des},create_date=NOW(),category_id=#{category.id},image=#{image} where id=#{id}")
	public boolean updateArticle(Article article);
	
	@Delete("delete from tbl_article where id=#{id}")
	public boolean deleteOneArticle(int id);

}
