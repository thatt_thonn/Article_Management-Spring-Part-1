package ArticleManagement.repository.category;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import ArticleManagement.model.Category;

@Repository
public interface CategoryRepository {
	
	@Insert("insert into tbl_category(name) values(#{name})")
	public void addCategory(Category category);
	
	@Select("select id, name from tbl_category where id=#{id}")
	@Results({
		@Result(property="id",column="id"),
		@Result(property="name",column="name")
	})
	public Category findOneCategory(Integer id);
	
	
	@Select("select id, name from tbl_category")
	@Results({
		@Result(property="id",column="id"),
		@Result(property="name",column="name")
	})
	public List<Category> findAll();
	
	@Update("update tbl_category set name=#{name} where id=#{id}")
	public boolean updateCategory(Category category);
	
	@Delete("delete from tbl_category where id=#{id}")
	public boolean deleteCategory(Integer id);

}
