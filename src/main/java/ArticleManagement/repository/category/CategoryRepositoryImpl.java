package ArticleManagement.repository.category;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import ArticleManagement.model.Category;

//@Repository
public class CategoryRepositoryImpl implements CategoryRepository {

	List<Category> list=new ArrayList<>();
	
	@Override
	public void addCategory(Category category) {
		 list.add(category);
	}

	@Override
	public Category findOneCategory(Integer id) {
		for(int i=0;i<list.size();i++) {
			if(list.get(i).getId()==id)
				return list.get(i);
		}
		return null;
	}

	@Override
	public List<Category> findAll() {
		return list;
	}

	@Override
	public boolean updateCategory(Category category) {
		for(int i=0;i<list.size();i++) {
			if(list.get(i).getId()==category.getId())
			{
				list.set(i, category);
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean deleteCategory(Integer id) {
		for(int i=0;i<list.size();i++) {
			if(list.get(i).getId()==id) {
				list.remove(i);
				return true;
			}
		}
		return false;
	}

}
