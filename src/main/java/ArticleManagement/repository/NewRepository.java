package ArticleManagement.repository;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class NewRepository {

	@Autowired
	private DataSource dataSource;
	
	public void show() {
		try {
			Connection con=dataSource.getConnection();
			if(con!=null) {
				System.out.println("Connected");
			}else {
				System.out.println("not Connect");
				return;
			}
			Statement st=con.createStatement();
			String sql="select * from tbl_category";
			ResultSet rs=st.executeQuery(sql);
			while(rs.next()) {
				System.out.println(rs.getString(1)+" : "+rs.getString(2));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
