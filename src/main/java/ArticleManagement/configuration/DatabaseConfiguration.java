package ArticleManagement.configuration;

import javax.sql.DataSource;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

@Configuration
//@MapperScan("ArticleManagement.repository")
public class DatabaseConfiguration {
	
	// for programme
	@Bean
	@Profile("database")
	public DataSource dataSource() {
		DriverManagerDataSource db=new DriverManagerDataSource();
		db.setDriverClassName("org.postgresql.Driver");
		db.setUrl("jdbc:postgresql://localhost:5432/spring_db");
		db.setUsername("postgres");
		db.setPassword("thon");
		return db;
	}
	
	
	@Bean
	@Profile("memory")
	public DataSource developMemory() {
		EmbeddedDatabaseBuilder db=new EmbeddedDatabaseBuilder();
		db.setType(EmbeddedDatabaseType.H2);
		db.addScript("sql/entity.sql");
		return db.build();
	}
	
	
	

}
