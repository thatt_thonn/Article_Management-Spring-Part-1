package ArticleManagement.configuration;

import java.util.Locale;

import org.apache.tomcat.util.descriptor.LocalResolver;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.web.servlet.LocaleContextResolver;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

@Configuration
//@EnableWebMvc
public class LanguageConfiguration extends WebMvcConfigurerAdapter {
	
	@Bean(name = "localeResolver")
	public SessionLocaleResolver localeResolver() {
		SessionLocaleResolver slr=new SessionLocaleResolver();		
		slr.setDefaultLocale(new Locale("kh"));
		return slr;
	}
	
	@Bean
	public LocaleChangeInterceptor localeChangeInterceptor() {
		LocaleChangeInterceptor obj=new LocaleChangeInterceptor();
		obj.setParamName("lang");
		return obj;
	}
	

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(this.localeChangeInterceptor());
	}
	
	@Bean
	public MessageSource messageSource() {
		ReloadableResourceBundleMessageSource msg=new ReloadableResourceBundleMessageSource();
		msg.setBasenames("classpath:languages/messages");
		return msg;
	}
}
