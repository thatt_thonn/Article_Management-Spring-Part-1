package ArticleManagement.configuration;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@PropertySource("classpath:location.properties")
public class FileUploadConfiguration extends WebMvcConfigurerAdapter{
	
	@Value("${client.img.path.folder}")
	private String clientImageFolder;
	
	@Value("${server.img.path.folder}")
	private String serverImagePathFolder;
	
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler(clientImageFolder+"**").addResourceLocations("file:"+serverImagePathFolder);
	}

}
