package ArticleManagement.configuration;

import java.util.Locale;

import javax.servlet.http.Cookie;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;

//@Configuration
//@EnableWebMvc
public class MultipleLanguage extends WebMvcConfigurerAdapter{
	
	// for set default language
	@Bean (name="localeResolver")
	public LocaleResolver localeResolver() {
		CookieLocaleResolver resolver=new CookieLocaleResolver();
		resolver.setDefaultLocale(new Locale("kh"));//resolver.setDefaultLocale(new Locale("kh"));
		resolver.setCookieName("Spring Locale");
		resolver.setCookieMaxAge(4800);
		return resolver;
	}
	// method can change language
	@Bean
	public LocaleChangeInterceptor localeChangeInterceptor() {
		LocaleChangeInterceptor lci=new LocaleChangeInterceptor();
		lci.setParamName("lang");
		return lci;
	}
	
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(this.localeChangeInterceptor());
	}
	
	@Bean
	public MessageSource messageSource() {
		ReloadableResourceBundleMessageSource smg=new ReloadableResourceBundleMessageSource();
		smg.setBasename("classpath:/languages/messages");
		smg.setCacheSeconds(0);
		smg.setUseCodeAsDefaultMessage(true);
		smg.setDefaultEncoding("UTF-8");
		return smg;
		
	}

}
