package ArticleManagement.service.category;

import java.util.List;

import ArticleManagement.model.Category;

public interface CategoryServices {
	void addCategory(Category category);
	Category findOneCategory(Integer id);
	
	List<Category> findAll();
	
	boolean updateCategory(Category category);
	boolean deleteCategory(Integer id);

}
