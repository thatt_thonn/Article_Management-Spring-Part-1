package ArticleManagement.service.category;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ArticleManagement.model.Category;
import ArticleManagement.repository.category.CategoryRepository;

@Service
public class CategoryServicesImpl implements CategoryServices {

	
	private CategoryRepository categoryRepository;
	@Autowired
	public void setCategoryRepository(CategoryRepository categoryRepository) {
		this.categoryRepository = categoryRepository;
	}
	
	@Override
	public void addCategory(Category category) {
		categoryRepository.addCategory(category);	
	}

	@Override
	public Category findOneCategory(Integer id) {	
		return categoryRepository.findOneCategory(id);
	}

	@Override
	public List<Category> findAll() {
		return categoryRepository.findAll();
	}

	@Override
	public boolean updateCategory(Category category) {
		return categoryRepository.updateCategory(category);
	}

	@Override
	public boolean deleteCategory(Integer id) {
		return categoryRepository.deleteCategory(id);
	}

}
