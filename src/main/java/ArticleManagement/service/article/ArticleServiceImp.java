package ArticleManagement.service.article;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ArticleManagement.model.Article;
import ArticleManagement.repository.articles.ArticleRepository;

@Service
public class ArticleServiceImp implements ArticleService {
	
	private ArticleRepository articleRepository;
	
	@Autowired
	public void setArticleRepository(ArticleRepository articleRepository) {
		this.articleRepository = articleRepository;
	}
	
	@Override
	public void addArticle(Article article) {
			articleRepository.addArticle(article);	
	}

	@Override
	public List<Article> findAllArticles() {
		return articleRepository.findAllArticles();
	}

	@Override
	public boolean updateArticle(Article article) {
		return articleRepository.updateArticle(article);
	}

	@Override
	public Article findArticleOne(int id) {
		return articleRepository.findArticleOne(id);
	}

	@Override
	public boolean deleteOneArticle(int id) {
		return articleRepository.deleteOneArticle(id);
	}
}
