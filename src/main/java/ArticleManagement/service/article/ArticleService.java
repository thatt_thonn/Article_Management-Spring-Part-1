package ArticleManagement.service.article;

import java.util.List;

import ArticleManagement.model.Article;

public interface ArticleService {
	void addArticle(Article article);
	List<Article> findAllArticles();
	
	Article findArticleOne(int id);
	boolean updateArticle(Article article);
	boolean deleteOneArticle(int id);
	
}
